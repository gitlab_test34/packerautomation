# Variables
$downloadfolder = 'C:\Users\davidh\Desktop\image_automation\packerautomation\packerautomationwindows\'

# Go to the Packer download folder
Set-Location $downloadfolder

# Show Packer Version
packer --version


# Download Packer plugins
packer init "${downloadfolder}windows_10.json.pkr.hcl"

# Packer build
packer build `
      --var 'disk_size=61440' `
      --var 'autounattend=./setup/Autounattend.xml' `
      .\windows_10.json.pkr.hcl