
variable "autounattend" {
  type    = string
  default = "./setup/Autounattend.xml"
}

variable "disk_size" {
  type    = string
  default = "61440"
}

variable "disk_type_id" {
  type    = string
  default = "1"
}

variable "headless" {
  type    = string
  default = "false"
}

variable "iso_checksum" {
  type    = string
  default = "EF7312733A9F5D7D51CFA04AC497671995674CA5E1058D5164D6028F0938D668"
}

variable "iso_url" {
  type    = string
  default = "C:/Users/davidh/Desktop/images/Windows10_Ent.iso"
}

variable "memory" {
  type    = string
  default = "2048"
}

variable "restart_timeout" {
  type    = string
  default = "5m"
}

variable "vhv_enable" {
  type    = string
  default = "false"
}

variable "virtio_win_iso" {
  type    = string
  default = "~/virtio-win.iso"
}

variable "vm_name" {
  type    = string
  default = "windows_10"
}

variable "vmx_version" {
  type    = string
  default = "19"
}

variable "winrm_timeout" {
  type    = string
  default = "6h"
}

source "vmware-iso" "test-vm" {
  boot_wait         = "5m"
  communicator      = "winrm"
  cpus              = 2
  disk_adapter_type = "nvme"
  disk_size         = "${var.disk_size}"
  disk_type_id      = "${var.disk_type_id}"
  floppy_files      = ["${var.autounattend}", "./setup/fixnetwork.ps1", "./setup/disable-screensaver.ps1", "./setup/disable-winrm.ps1", "./setup/enable-winrm.ps1", "./setup/microsoft-updates.bat", "./setup/win-updates.ps1", "./setup/vm-guest-tools.ps1", "./setup/enable-rdp.bat"]
  guest_os_type     = "windows9-64"
  headless          = "${var.headless}"
  iso_checksum      = "${var.iso_checksum}"
  iso_url           = "${var.iso_url}"
  memory            = "${var.memory}"
  shutdown_command  = "shutdown /s /t 10 /f /d p:4:1 /c \"Packer Shutdown\""
  version           = "${var.vmx_version}"
  vm_name           = "${var.vm_name}"
  vmx_data = {
    "RemoteDisplay.vnc.enabled" = "false"
    "RemoteDisplay.vnc.port"    = "5900"
  }
  vmx_remove_ethernet_interfaces = true
  vnc_port_max                   = 5980
  vnc_port_min                   = 5900
  winrm_password                 = "vagrant"
  winrm_timeout                  = "${var.winrm_timeout}"
  winrm_username                 = "vagrant"
}

build {
  sources = ["source.vmware-iso.test-vm"]

  provisioner "windows-shell" {
    execute_command = "{{ .Vars }} cmd /c \"{{ .Path }}\""
    remote_path     = "/tmp/script.bat"
    scripts         = ["./setup/enable-rdp.bat"]
  }

  provisioner "powershell" {
    scripts = ["./setup/vm-guest-tools.ps1", "./setup/debloat-windows.ps1"]
  }

  provisioner "windows-restart" {
    restart_timeout = "${var.restart_timeout}"
  }

  provisioner "powershell" {
    scripts = ["./setup/set-powerplan.ps1", "./setup/disable-windows-defender.ps1"]
  }

  provisioner "windows-shell" {
    execute_command = "{{ .Vars }} cmd /c \"{{ .Path }}\""
    remote_path     = "/tmp/script.bat"
    scripts         = ["./setup/pin-powershell.bat", "./setup/compile-dotnet-assemblies.bat", "./setup/set-winrm-automatic.bat", "./setup/uac-enable.bat", "./setup/dis-updates.bat", "./setup/compact.bat"]
  }

  post-processor "vagrant" {
    keep_input_artifact  = false
    output               = "windows_10_{{ .Provider }}.box"
    vagrantfile_template = "vagrantfile-windows_10.template"
  }
}
