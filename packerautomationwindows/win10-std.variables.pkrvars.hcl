// VM
vm_name                 = "test-vm"
winrm_username          = "vagrant"
winrm_password          = "vagrant" // maybe works
operating_system_vm     = "windows9-64"
source_name             = "source.vmware-iso.test-vm"
vm_firmware             = "bios"
vm_cdrom_type           = "sata"
vm_cpus                 = "2"
vm_cores                = "1"
vm_memory               = "2048"
vm_disk_controller_type = "nvme"
vm_disk_size            = "32768"
vm_network_adapter_type = "e1000e"
// Use the NAT Network
vm_network         = "VMnet8"
vm_hardwareversion = "19"

// Removeable media
iso_url = "C:/Users/davidh/Desktop/images/10240.16384.150709-1700.TH1_CLIENTENTERPRISEEVAL_OEMRET_X64FRE_EN-US.ISO"
// In Powershell use the "get-filehash" command to find the checksum of the ISO
iso_checksum = "9B53E1F6AB6259A05D0E8F42DB5BAAD50B57DA34DA9312783F7E8BD4AE2CD3CA56ab095075be28a90bc0b510835280975c6bb2ce"
